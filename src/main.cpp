#include "sensor/bma2x2.h"
#include "sensor/bmg160.h"
#include "sensor/bmm050.h"
#include <Arduino.h>
#include <CapacitiveSensor.h>
#include <Wire.h>

struct bmm050_t mag = {0};
struct bma2x2_t accel = {0};
struct bmg160_t gyro = {0};

void fake_delay(unsigned int ms) {
  delay(ms);
}

s8 bus_write(u8 dev_addr, u8 reg_addr, u8 *reg_data, u8 cnt) {
  Wire.beginTransmission(dev_addr);
  Wire.write(reg_addr);
  for (int i = 0; i < cnt; i++)
    Wire.write(reg_data[i]);
  return Wire.endTransmission();
}

s8 bus_read(u8 dev_addr, u8 reg_addr, u8 *reg_data, u8 cnt) {
  Wire.beginTransmission(dev_addr);
  Wire.write(reg_addr);
  auto res = Wire.endTransmission(false);
  if (res != 0)
    return res;
  uint8_t i = 0;
  Wire.requestFrom(dev_addr, cnt);
  while (Wire.available() && i < cnt) {
    reg_data[i] = Wire.read();
    i++;
  }
  return res;
}

int prepare_mag(bmm050_t &mag_cfg) {
  int com_rslt = 0;

  mag_cfg.dev_addr = 0x10;
  mag_cfg.delay_msec = &fake_delay;
  mag_cfg.bus_write = &bus_write;
  mag_cfg.bus_read = &bus_read;
  com_rslt = bmm050_init(&mag_cfg);
  if (com_rslt != 0)
    return com_rslt;

  com_rslt = bmm050_set_functional_state(BMM050_NORMAL_MODE);
  if (com_rslt != 0)
    return com_rslt;

  auto v_data_rate_value_u8 = BMM050_DATA_RATE_30HZ; // set data rate of 30Hz
  com_rslt = bmm050_set_data_rate(v_data_rate_value_u8);
  if (com_rslt != 0)
    return com_rslt;

  u8 v_data_rate_u8 = BMM050_INIT_VALUE;
  com_rslt = bmm050_get_data_rate(&v_data_rate_u8);

  return com_rslt;
}

int prepare_accel(bma2x2_t &accel_cfg) {
  int com_rslt = 0;

  accel_cfg.dev_addr = 0x68;
  accel_cfg.delay_msec = &fake_delay;
  accel_cfg.bus_write = &bus_write;
  accel_cfg.bus_read = &bus_read;
  com_rslt = bma2x2_init(&accel_cfg);
  if (com_rslt != 0)
    return com_rslt;

  com_rslt = bma2x2_set_power_mode(BMA2x2_MODE_NORMAL);
  if (com_rslt != 0)
    return com_rslt;

  auto bw_value_u8 = 0x08; // set bandwidth of 7.81Hz
  com_rslt = bma2x2_set_bw(bw_value_u8);
  if (com_rslt != 0)
    return com_rslt;

  u8 bw_u8 = 0;
  com_rslt = bma2x2_get_bw(&bw_u8);

  return com_rslt;
}

int prepare_gyro(bmg160_t &gyro_cfg) {
  int com_rslt = 0;

  gyro_cfg.dev_addr = 0x18;
  gyro_cfg.delay_msec = &fake_delay;
  gyro_cfg.bus_write = &bus_write;
  gyro_cfg.bus_read = &bus_read;

  com_rslt = bmg160_init(&gyro_cfg);
  if (com_rslt != 0)
    return com_rslt;

  com_rslt = bmg160_set_power_mode(BMG160_MODE_NORMAL);
  if (com_rslt != 0)
    return com_rslt;

  auto v_bw_u8 = C_BMG160_BW_230HZ_U8X; // set gyro bandwidth of 230Hz
  com_rslt += bmg160_set_bw(v_bw_u8);
  if (com_rslt != 0)
    return com_rslt;

  u8 v_gyro_value_u8 = 0;
  com_rslt += bmg160_get_bw(&v_gyro_value_u8);

  return com_rslt;
}

__unused void setup() {
  Wire.begin();
  Serial.begin(9600);
  while (!Serial)
    delay(100);

  Serial.println(">>>>");
  Serial.println(prepare_mag(mag));
  Serial.println(prepare_accel(accel));
  Serial.println(prepare_gyro(gyro));
  Serial.println("<<<<");
}

__unused void loop() {
  struct bmm050_mag_data_s16_t mag_data = {0};
  struct bma2x2_accel_data accel_data = {0};
  struct bmg160_data_t gyro_data = {0};


  Serial.println(">>>> MAG <<<<");
  Serial.println(bmm050_read_mag_data_XYZ(&mag_data));
  Serial.println(mag_data.datax);
  Serial.println(mag_data.datay);
  Serial.println(mag_data.dataz);

  Serial.println(">>>> ACL <<<<");
  Serial.println(bma2x2_read_accel_xyz(&accel_data));
  Serial.println(accel_data.x);
  Serial.println(accel_data.y);
  Serial.println(accel_data.z);

  Serial.println(">>>> GYR <<<<");
  Serial.println(bmg160_get_data_XYZI(&gyro_data));
  Serial.println(gyro_data.datax);
  Serial.println(gyro_data.datay);
  Serial.println(gyro_data.dataz);
  Serial.println(gyro_data.intstatus);

  Serial.println(">>>> DONE <<<<");
  delay(500);
}
