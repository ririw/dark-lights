#include <climits>
//
// Created by Richard Weiss on 2019-04-23.
//

#ifndef DARK_LIGHTS_IMAGES_H
#define DARK_LIGHTS_IMAGES_H

#include <FastLED.h>
#include <stdint.h>
static const CRGB b = CRGB::Black;
static const CRGB y = CRGB::Yellow;
static const CRGB r = CRGB::Red;
static const CRGB w = CRGB::SteelBlue;
static const CRGB g = CRGB::Green;
static const CRGB l = CRGB::Blue;
static const CRGB gy = CRGB(10, 10, 10);
static const CRGB br = CRGB(0xa56729);
static const CRGB o = CRGB(0xFFA500);

typedef struct ImageC {
  int type_note;
  void (*fill_img_buffer)(ImageC *self, CRGB buffer[8][8]);
  ImageC *(*next_img)(ImageC *self);
  uint8_t (*dt)(ImageC *self);
  void (*cleanup)(ImageC *self);
  void *class_data;
} ImageC;

const static int COLORED_IMAGE_TYPE = 0;
const static int KEYFRAME_IMAGE_TYPE = 1;
const static int HELIX_IMAGE_TYPE = 2;
const static int BREAKER_IMAGE_TYPE = 3;
const static int FIRE_IMAGE_TYPE = 4;

ImageC *no_next_image(ImageC __unused *self) { return nullptr; }

uint8_t one_step(ImageC __unused *self) { return 31; }

void free_class_data(ImageC *self) { free(self->class_data); }

/**  MONO IMAGES */
typedef struct ColoredImageData {
  uint8_t image_data[8][8];
  CRGB color;
  __unused int ix;
} ColoredImageData;

void check_type_colored(ImageC *self) {
  if (self->type_note != COLORED_IMAGE_TYPE) {
    Serial.printf(
        "Warning - got %d rather than COLORED_IMAGE_TYPE in colored image",
        self->type_note);
  }
}

void fill_img_buffer_colored_img(ImageC *self, CRGB buffer[8][8]) {
  check_type_colored(self);
  auto data = (ColoredImageData *)self->class_data;

  for (int i = 0; i < 8; i++) {
    for (int j = 0; j < 8; j++) {
      buffer[i][j] = data->image_data[i][j] ? data->color : CRGB::Black;
    }
  }
}

ImageC *CColoredImage(uint8_t im[8][8], const CRGB &color, int ix) {
  auto img = (ImageC *)malloc(sizeof(ImageC));
  img->type_note = COLORED_IMAGE_TYPE;
  auto class_data = (ColoredImageData *)malloc(sizeof(ColoredImageData));
  img->class_data = (void *)class_data;
  img->fill_img_buffer = &fill_img_buffer_colored_img;
  img->next_img = &no_next_image;
  img->dt = &one_step;
  img->cleanup = &free_class_data;

  class_data->ix = ix;
  class_data->color = color;

  for (int i = 0; i < 8; i++) {
    for (int j = 0; j < 8; j++) {
      class_data->image_data[i][j] = im[i][j];
    }
  }

  return img;
}

/**  KEYFRAME IMAGES */

typedef CRGB Img[8][8];

typedef struct CKeyframedData {
  int num_imgs;
  int frame_ix;
  int im_ix;
  int repeats;
  Img *frames;
} CKeyframedData;

CKeyframedData *get_check_data(ImageC *self) {
  return (CKeyframedData *)(self->class_data);
}

void free_c_keyframed_data(ImageC *self) {
  auto data = (CKeyframedData *)(self->class_data);
  free(data->frames);
  free(data);
  free(self);
}

void fill_img_buffer_keyframe(ImageC *self, CRGB buffer[8][8]) {
  auto data = (CKeyframedData *)self->class_data;
  int frame_ix = data->frame_ix % data->num_imgs;

  for (int i = 0; i < 8; i++) {
    for (int j = 0; j < 8; j++) {
      buffer[i][j] = data->frames[frame_ix][i][j];
    }
  }
}

ImageC *next_kf(ImageC *self);

ImageC *CKeyframedImage(CRGB ims[][8][8], int num_imgs, int im_ix, int frame_ix,
                        int repeats) {
  auto img = (ImageC *)malloc(sizeof(ImageC));
  img->type_note = KEYFRAME_IMAGE_TYPE;
  img->fill_img_buffer = &fill_img_buffer_keyframe;
  img->next_img = &next_kf;
  img->dt = [](ImageC *self) -> uint8_t { return 2; };
  img->cleanup = &free_c_keyframed_data;

  auto class_data = (CKeyframedData *)malloc(sizeof(CKeyframedData));
  img->class_data = (void *)class_data;

  class_data->num_imgs = num_imgs;
  class_data->im_ix = im_ix;
  class_data->repeats = repeats;
  class_data->frame_ix = frame_ix;
  class_data->frames = (Img *)malloc(sizeof(Img) * num_imgs);

  for (int i = 0; i < num_imgs; i++) {
    for (int j = 0; j < 8; j++) {
      for (int k = 0; k < 8; k++) {
        class_data->frames[i][j][k] = ims[i][j][k];
      }
    }
  }

  return img;
}

ImageC *next_kf(ImageC *self) {
  auto data = get_check_data(self);

  if (data->frame_ix + 1 >= data->num_imgs * data->repeats) {
    return nullptr;
  } else {
    return CKeyframedImage(data->frames, data->num_imgs, data->im_ix,
                           data->frame_ix + 1, data->repeats);
  }
}

/** HELIX */

typedef struct HelixData {
  int frame_ix;
} HelixData;

HelixData *get_helix(ImageC *self) { return (HelixData *)self->class_data; }

void fill_img_buffer_helix(ImageC *self, CRGB buffer[8][8]) {
  auto helix_data = get_helix(self);
  static CRGB helix_image[8][8] = {
      {b, b, b, l, r, b, b, b}, {b, b, l, b, b, r, b, b},
      {b, b, l, b, b, r, b, b}, {b, b, b, l, r, b, b, b},
      {b, b, b, r, l, b, b, b}, {b, b, r, b, b, l, b, b},
      {b, b, r, b, b, l, b, b}, {b, b, b, r, l, b, b, b},
  };

  for (int i = 0; i < 8; i++) {
    for (int j = 0; j < 8; j++) {
      buffer[i][j] = helix_image[(i + helix_data->frame_ix) % 8][j];
    }
  }
}

ImageC *next_helix(ImageC *self);

void free_helix(ImageC *self) {
  free(self->class_data);
  free(self);
}

ImageC *Helix(int frame_ix) {
  auto img = (ImageC *)malloc(sizeof(ImageC));
  img->type_note = HELIX_IMAGE_TYPE;
  img->fill_img_buffer = &fill_img_buffer_helix;
  img->next_img = &next_helix;
  img->dt = [](ImageC *self) -> uint8_t { return 2; };
  img->cleanup = &free_helix;

  auto class_data = (HelixData *)malloc(sizeof(HelixData));
  img->class_data = (void *)class_data;

  class_data->frame_ix = frame_ix;
  return img;
}

ImageC *next_helix(ImageC *self) {
  auto helix_data = get_helix(self);
  if (helix_data->frame_ix > 32) {
    return nullptr;
  } else {
    return Helix(helix_data->frame_ix + 1);
  }
}

/** BREAKER */

static CRGB breaker_color = CRGB(0xFF0000);

ImageC *Breaker(bool randomize_at_cleanup) {
  auto img = (ImageC *)malloc(sizeof(ImageC));
  img->type_note = BREAKER_IMAGE_TYPE;
  img->fill_img_buffer = [](ImageC *self, Img buffer) -> void {
    for (int i = 0; i < 8; i++) {
      for (int j = 0; j < 8; j++) {
        buffer[i][j] = breaker_color;
      }
    }
  };
  img->dt = &one_step;

  if (!randomize_at_cleanup) {
    img->next_img = [](ImageC *self) -> ImageC * { return Breaker(true); };
    img->cleanup = [](ImageC *self) -> void { free(self); };
  } else {
    img->next_img = &no_next_image;
    img->cleanup = [](ImageC *self) -> void {
      free(self);
      breaker_color = CRGB(random8(), random8(), random8());
    };
  }
  img->class_data = nullptr;

  return img;
}

/** FIRE */

typedef struct FireData {
  uint8_t heat_lines[8][8];
  uint32_t countdown;
} FireData;

void cleanup_fire(ImageC *self) {
  free(self->class_data);
  free(self);
}

void fire_fill_buffer(ImageC *self, Img buffer);
ImageC *fire_next(ImageC *self);

ImageC *Fire(uint32_t countdown_to_done) {
  auto img = (ImageC *)malloc(sizeof(ImageC));
  img->type_note = FIRE_IMAGE_TYPE;
  img->dt = [](ImageC *self) -> uint8_t { return 1; };
  img->cleanup = &cleanup_fire;
  img->fill_img_buffer = &fire_fill_buffer;
  img->next_img = &fire_next;

  auto data = (FireData *)malloc(sizeof(FireData));
  data->countdown = countdown_to_done;
  for (auto &fire_line : data->heat_lines) {
    for (uint8_t &j : fire_line) {
      j = 0;
    }
  }
  img->class_data = data;

  return img;
}

void fire_fill_buffer(ImageC *self, Img buffer) {
  auto fire_data = (FireData *)self->class_data;
  for (int line = 0; line < 8; line++) {
    auto cells = fire_data->heat_lines[line];
    for (int j = 0; j < 8; j++) {
      buffer[7 - j][line] = HeatColor(cells[j]);
    }
  }
}

#define COOLING 120
#define SPARKING 150
ImageC *fire_next(ImageC *self) {
  auto fire_data = (FireData *)self->class_data;
  if (fire_data->countdown == 0)
    return nullptr;

  auto new_fire = Fire(fire_data->countdown - 1);
  auto new_fire_data = (FireData *)new_fire->class_data;

  for (int line = 0; line < 8; line++) {
    auto cells = new_fire_data->heat_lines[line];

    // Step 0. copy across the data from the old fire...
    for (int cell = 0; cell < 8; cell++) {
      cells[cell] = fire_data->heat_lines[line][cell];
    }

    // Step 1. Cool down every cell a little
    for (uint8_t cell = 0; cell < 8; cell++) {
      cells[cell] = qsub8(cells[cell], random8(0, ((COOLING * 10) / 8) + 2));
    }
    // Step 2. Heat from each cell drifts 'up' and diffuses a little
    for (int cell = 8 - 1; cell >= 2; cell--) {
      cells[cell] = (cells[cell - 1] + cells[cell - 2] + cells[cell - 2]) / 3;
    }
    // Step 3. Randomly ignite new 'sparks' of heat near the bottom
    if (random8() < SPARKING) {
      int z = random8(2);
      cells[z] = qadd8(cells[z], random8(160, 255));
    }
  }

  return new_fire;
}

CRGB _heart[6][8][8] = {
    {{b, b, b, b, b, b, b, b},
     {b, b, r, b, b, r, b, b},
     {b, r, b, r, r, b, r, b},
     {b, r, b, b, b, b, r, b},
     {b, b, r, b, b, r, b, b},
     {b, b, b, r, r, b, b, b},
     {b, b, b, b, b, b, b, b},
     {b, b, b, b, b, b, b, b}},
    {{b, b, b, b, b, b, b, b},
     {b, b, r, b, b, r, b, b},
     {b, r, r, r, r, r, r, b},
     {b, r, r, r, r, r, r, b},
     {b, b, r, r, r, r, b, b},
     {b, b, b, r, r, b, b, b},
     {b, b, b, b, b, b, b, b},
     {b, b, b, b, b, b, b, b}},
    {{b, b, b, b, b, b, b, b},
     {b, b, r, b, b, r, b, b},
     {b, r, b, r, r, b, r, b},
     {b, r, b, b, b, b, r, b},
     {b, b, r, b, b, r, b, b},
     {b, b, b, r, r, b, b, b},
     {b, b, b, b, b, b, b, b},
     {b, b, b, b, b, b, b, b}},
    {{b, b, b, b, b, b, b, b},
     {b, b, r, b, b, r, b, b},
     {b, r, r, r, r, r, r, b},
     {b, r, r, r, r, r, r, b},
     {b, b, r, r, r, r, b, b},
     {b, b, b, r, r, b, b, b},
     {b, b, b, b, b, b, b, b},
     {b, b, b, b, b, b, b, b}},
    {{b, b, b, b, b, b, b, b},
     {b, b, r, b, b, r, b, b},
     {b, r, b, r, r, b, r, b},
     {b, r, b, b, b, b, r, b},
     {b, b, r, b, b, r, b, b},
     {b, b, b, r, r, b, b, b},
     {b, b, b, b, b, b, b, b},
     {b, b, b, b, b, b, b, b}},
    {{b, b, b, b, b, b, b, b},
     {b, b, r, b, b, r, b, b},
     {b, r, b, r, r, b, r, b},
     {b, r, b, b, b, b, r, b},
     {b, b, r, b, b, r, b, b},
     {b, b, b, r, r, b, b, b},
     {b, b, b, b, b, b, b, b},
     {b, b, b, b, b, b, b, b}},
};

uint8_t _cross[8][8] = {
    {0, 0, 0, 1, 1, 0, 0, 0}, {0, 0, 0, 1, 1, 0, 0, 0},
    {0, 0, 0, 1, 1, 0, 0, 0}, {1, 1, 1, 1, 1, 1, 1, 1},
    {1, 1, 1, 1, 1, 1, 1, 1}, {0, 0, 0, 1, 1, 0, 0, 0},
    {0, 0, 0, 1, 1, 0, 0, 0}, {0, 0, 0, 1, 1, 0, 0, 0},
};

uint8_t _bolt[8][8] = {{0, 0, 0, 0, 1, 1, 0, 0}, {0, 0, 0, 1, 1, 0, 0, 0},
                       {0, 0, 1, 1, 0, 0, 0, 0}, {0, 0, 0, 1, 1, 0, 0, 0},
                       {0, 0, 0, 0, 1, 1, 0, 0}, {0, 0, 0, 1, 1, 0, 0, 0},
                       {0, 0, 1, 1, 0, 0, 0, 0}, {0, 1, 1, 0, 0, 0, 0, 0}};

uint8_t _face_happy[8][8] = {
    {0, 0, 0, 0, 0, 0, 0, 0}, {0, 1, 1, 0, 0, 1, 1, 0},
    {0, 1, 1, 0, 0, 1, 1, 0}, {0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0}, {0, 0, 1, 0, 0, 1, 0, 0},
    {0, 0, 0, 1, 1, 0, 0, 0}, {0, 0, 0, 0, 0, 0, 0, 0}};

uint8_t _arrow[8][8] = {{0, 0, 0, 1, 1, 0, 0, 0}, {0, 0, 1, 1, 1, 1, 0, 0},
                        {0, 1, 1, 1, 1, 1, 1, 0}, {0, 0, 0, 1, 1, 0, 0, 0},
                        {0, 0, 0, 1, 1, 0, 0, 0}, {0, 0, 0, 1, 1, 0, 0, 0},
                        {0, 0, 0, 1, 1, 0, 0, 0}, {0, 0, 0, 1, 1, 0, 0, 0}};

uint8_t _chess[8][8] = {{1, 0, 1, 0, 1, 0, 1, 0}, {0, 1, 0, 1, 0, 1, 0, 1},
                        {1, 0, 1, 0, 1, 0, 1, 0}, {0, 1, 0, 1, 0, 1, 0, 1},
                        {1, 0, 1, 0, 1, 0, 1, 0}, {0, 1, 0, 1, 0, 1, 0, 1},
                        {1, 0, 1, 0, 1, 0, 1, 0}, {0, 1, 0, 1, 0, 1, 0, 1}};

uint8_t _dance1[8][8] = {{0, 0, 1, 1, 1, 0, 0, 0}, {1, 0, 1, 1, 1, 0, 0, 0},
                         {0, 1, 0, 1, 0, 0, 0, 0}, {0, 0, 1, 1, 1, 1, 1, 0},
                         {0, 0, 0, 1, 0, 0, 0, 0}, {0, 0, 1, 0, 1, 0, 0, 0},
                         {0, 1, 0, 0, 0, 1, 0, 0}, {0, 1, 0, 0, 0, 1, 0, 0}};

uint8_t _dance2[8][8] = {{0, 0, 1, 1, 1, 0, 0, 0}, {0, 0, 1, 1, 1, 0, 1, 0},
                         {0, 0, 0, 1, 0, 1, 0, 0}, {1, 1, 1, 1, 1, 0, 0, 0},
                         {0, 0, 0, 1, 0, 0, 0, 0}, {0, 0, 1, 0, 1, 0, 0, 0},
                         {0, 1, 0, 0, 0, 1, 0, 0}, {0, 1, 0, 0, 0, 1, 0, 0}};

Img _dancer[2] = {{{b, b, r, r, r, b, b, b},
                       {r, b, r, r, r, b, b, b},
                       {b, r, b, r, b, b, b, b},
                       {b, b, r, r, r, r, r, b},
                       {b, b, b, r, b, b, b, b},
                       {b, b, r, b, r, b, b, b},
                       {b, r, b, b, b, r, b, b},
                       {b, b, b, b, b, r, b, b}},
                      {{b, b, r, r, r, b, b, b},
                       {b, b, r, r, r, b, r, b},
                       {b, b, b, r, b, r, b, b},
                       {r, r, r, r, r, b, b, b},
                       {b, b, b, r, b, b, b, b},
                       {b, b, r, b, r, b, b, b},
                       {b, r, b, b, b, r, b, b},
                       {b, r, b, b, b, b, b, b}}};

CRGB _lines[12][8][8] = {
    {{y, y, y, y, y, y, y, y}, // --
     {b, b, b, b, b, b, b, b},
     {b, b, b, b, b, b, b, b},
     {b, b, b, b, b, b, b, b},
     {b, b, b, b, b, b, b, b},
     {b, b, b, b, b, b, b, b},
     {b, b, b, b, b, b, b, b},
     {b, b, b, b, b, b, b, b}},
    {{b, b, b, b, b, b, b, b}, // --
     {y, y, y, y, y, y, y, y}, //
     {b, b, b, b, b, b, b, b},
     {b, b, b, b, b, b, b, b},
     {b, b, b, b, b, b, b, b},
     {b, b, b, b, b, b, b, b},
     {b, b, b, b, b, b, b, b},
     {b, b, b, b, b, b, b, b}},
    {{b, b, b, b, b, b, b, b}, // --
     {b, b, b, b, b, b, b, b},
     {y, y, y, y, y, y, y, y}, //
     {b, b, b, b, b, b, b, b},
     {b, b, b, b, b, b, b, b},
     {b, b, b, b, b, b, b, b},
     {b, b, b, b, b, b, b, b},
     {b, b, b, b, b, b, b, b}},
    {{b, b, b, b, b, b, b, b}, // --
     {b, b, b, b, b, b, b, b},
     {b, b, b, b, b, b, b, b},
     {y, y, y, y, y, y, y, y}, //
     {b, b, b, b, b, b, b, b},
     {b, b, b, b, b, b, b, b},
     {b, b, b, b, b, b, b, b},
     {b, b, b, b, b, b, b, b}},
    {{b, b, b, b, b, b, b, b}, // --
     {b, b, b, b, b, b, b, b},
     {b, b, b, b, b, b, b, b},
     {b, b, b, b, b, b, b, b},
     {y, y, y, y, y, y, y, y}, //
     {b, b, b, b, b, b, b, b},
     {b, b, b, b, b, b, b, b},
     {b, b, b, b, b, b, b, b}},
    {{b, b, b, b, b, b, b, b}, // --
     {b, b, b, b, b, b, b, b},
     {b, b, b, b, b, b, b, b},
     {b, b, b, b, b, b, b, b},
     {b, b, b, b, b, b, b, b},
     {y, y, y, y, y, y, y, y}, //
     {b, b, b, b, b, b, b, b},
     {b, b, b, b, b, b, b, b}},
    {{b, b, b, b, b, b, b, b}, // --
     {b, b, b, b, b, b, b, b},
     {b, b, b, b, b, b, b, b},
     {b, b, b, b, b, b, b, b},
     {b, b, b, b, b, b, b, b},
     {b, b, b, b, b, b, b, b},
     {y, y, y, y, y, y, y, y}, //
     {b, b, b, b, b, b, b, b}},
    {{b, b, b, b, b, b, b, b}, // --
     {b, b, b, b, b, b, b, b},
     {b, b, b, b, b, b, b, b},
     {b, b, b, b, b, b, b, b},
     {b, b, b, b, b, b, b, b},
     {b, b, b, b, b, b, b, b},
     {b, b, b, b, b, b, b, b},
     {y, y, y, y, y, y, y, y}}, //
    {{b, b, b, b, b, b, b, b},  // --
     {b, b, b, b, b, b, b, b},
     {b, b, b, b, b, b, b, b},
     {b, b, b, b, b, b, b, b},
     {b, b, r, b, b, b, b, r},
     {b, r, y, b, b, r, r, r},
     {y, y, y, y, r, y, y, y},
     {y, y, y, y, y, y, y, y}}, //
    {{b, b, b, b, b, b, b, b},  // --
     {b, b, b, b, b, b, b, b},
     {b, b, b, b, b, b, b, b},
     {b, b, r, b, b, b, r, b},
     {b, r, r, b, b, r, y, r},
     {b, r, y, y, b, y, y, r},
     {y, y, y, y, r, y, y, y},
     {y, y, y, y, y, y, y, y}}, //
    {{b, b, b, b, b, b, b, b},  // --
     {b, b, b, b, b, b, b, b},
     {b, b, b, b, b, b, b, b},
     {b, b, r, b, b, b, r, b},
     {b, r, r, b, b, r, y, r},
     {b, r, y, y, b, y, y, r},
     {y, y, y, y, r, y, y, y},
     {y, y, y, y, y, y, y, y}}, //
    {{b, b, b, b, b, b, b, r},  // --
     {b, b, b, b, r, b, r, r},
     {b, b, y, b, r, r, y, r},
     {r, r, y, r, y, y, y, y},
     {y, y, y, y, y, r, y, y},
     {y, y, y, y, y, y, y, y},
     {y, y, y, y, r, y, y, y},
     {y, y, y, y, y, y, y, y}}, //
};

uint8_t _zima[8][8] = {{0, 0, 0, 0, 0, 0, 0, 0}, {0, 0, 0, 0, 0, 0, 0, 0},
                       {0, 0, 1, 1, 1, 1, 0, 0}, {0, 0, 1, 1, 1, 1, 0, 0},
                       {0, 0, 1, 1, 1, 1, 0, 0}, {0, 0, 1, 1, 1, 1, 0, 0},
                       {0, 0, 0, 0, 0, 0, 0, 0}, {0, 0, 0, 0, 0, 0, 0, 0}};

CRGB image[3][8][8] = {{{b, b, y, y, y, y, y, b},
                        {b, y, b, b, b, b, b, y},
                        {y, b, b, b, w, b, y, b},
                        {y, b, b, b, b, y, b, b},
                        {y, b, b, b, y, b, b, r},
                        {y, b, b, b, b, y, y, b},
                        {b, y, b, b, b, b, b, y},
                        {b, b, y, y, y, y, y, b}},
                       {{b, b, y, y, y, y, y, b},
                        {b, y, b, b, b, b, b, y},
                        {y, b, b, b, w, b, b, y},
                        {y, b, b, b, b, y, y, b},
                        {y, b, b, b, y, b, r, b},
                        {y, b, b, b, b, y, y, b},
                        {b, y, b, b, b, b, b, y},
                        {b, b, y, y, y, y, y, b}},
                       {{b, b, y, y, y, y, y, b},
                        {b, y, b, b, b, b, b, y},
                        {y, b, b, b, w, b, b, y},
                        {y, b, b, b, b, b, b, y},
                        {y, b, b, b, y, y, y, b},
                        {y, b, b, b, b, b, b, y},
                        {b, y, b, b, b, b, b, y},
                        {b, b, y, y, y, y, y, b}}};

CRGB _ghost[3][8][8] = {{{b, b, b, g, g, g, b, b},
                         {b, b, g, g, g, g, g, b},
                         {b, g, b, b, g, b, b, g},
                         {b, g, b, b, g, b, b, g},
                         {b, g, g, g, g, g, g, g},
                         {b, g, g, g, g, g, g, g},
                         {b, g, g, g, g, g, g, g},
                         {b, g, b, g, b, g, b, g}},
                        {{b, b, g, g, g, b, b, b},
                         {b, g, g, g, g, g, b, b},
                         {g, b, b, g, b, b, g, b},
                         {g, b, b, g, b, b, g, b},
                         {g, g, g, g, g, g, g, b},
                         {g, g, g, g, g, g, g, b},
                         {g, g, g, g, g, g, g, b},
                         {g, b, g, b, g, b, g, b}}};

Img _pika[1] = {{
    {b, gy, gy, b, b, b, b, gy},
    {b, b, y, o, b, b, b, o},
    {b, b, b, y, y, y, y, o},
    {o, o, b, y, l, y, y, l},
    {o, o, b, r, y, y, y, o},
    {b, br, b, y, o, o, o, b},
    {b, br, y, o, y, o, y, b},
    {b, b, y, o, br, br, o, b},
}};

ImageC *cross = CColoredImage(_cross, CRGB::Red, 1);
ImageC *bolt = CColoredImage(_bolt, CRGB::Yellow, 2);
ImageC *face_happy = CColoredImage(_face_happy, CRGB::Yellow, 3);
ImageC *arrow = CColoredImage(_arrow, CRGB::Green, 4);
ImageC *chess = CColoredImage(_chess, CRGB::Red, 5);
ImageC *dance1 = CColoredImage(_dance1, CRGB::Red, 6);
ImageC *dance2 = CColoredImage(_dance2, CRGB::Red, 7);
ImageC *zima = CColoredImage(_zima, CRGB(0x5BC2E7), 10);

ImageC *helix = Helix(0);

ImageC *pacman = CKeyframedImage(image, 3, 0, 0, 6);
ImageC *roller = CKeyframedImage(_lines, 12, 0, 0, 1);
ImageC *heart = CKeyframedImage(_heart, 6, 0, 0, 6);
ImageC *ghost = CKeyframedImage(_ghost, 2, 0, 0, 6);
ImageC *pika = CKeyframedImage(_pika, 1, 0, 0, 12);
ImageC *idance = CKeyframedImage(_dancer, 2, 0, 0, 12);

ImageC *breaker = Breaker(false);
ImageC *fire = Fire(128);

static const int num_imgs = 33;
ImageC *images[num_imgs] = {
    fire,    heart,   breaker, cross,   breaker, bolt,    breaker, face_happy,
    breaker, arrow,   breaker, chess,   breaker, dance1,  dance2,  dance1,
    dance2,  idance, breaker,
    cross,   zima,    breaker, pacman,  breaker, roller,  cross,   breaker,
    ghost,   breaker, helix,   breaker, cross,   pika};
#endif // DARK_LIGHTS_IMAGES_H
