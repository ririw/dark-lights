//
// Created by Richard Weiss on 2019-04-18.
//

#ifndef DARK_LIGHTS_IMAGE_TOOLS_H
#define DARK_LIGHTS_IMAGE_TOOLS_H

#include <Arduino.h>
#include <FastLED.h>

void fade_over(uint8_t size, CRGB dest[], CRGB src1[], CRGB src2[],
               uint8_t fade_lvl) {
  for (int i = 0; i < size; i++) {
    CRGB &src1_faded = src1->fadeToBlackBy(fade_lvl);
    CRGB &src2_faded = src2->fadeLightBy(255 - fade_lvl);
    dest[i] = src1_faded + src2_faded;
  }
}

template <uint8_t R, uint8_t C> class ImTools {
private:
  CRGB *underlying;

public:
  explicit ImTools(CRGB *u) { underlying = u; }

  void fade_between(ImTools<R, C> from_img, ImTools<R, C> to_img, uint8_t fade_lvl) {
    for (int i = 0; i < R * C; i++) {
      CRGB src1_faded = from_img.underlying[i].fadeLightBy(255 - fade_lvl);
      CRGB src2_faded = to_img.underlying[i].fadeLightBy(fade_lvl);
      underlying[i] = src1_faded + src2_faded;
    }
  }

  void render_into_img(uint8_t img[R][C], const CRGB &color) {
    for (uint8_t r = 0; r < R; r++) {
      for (uint8_t c = 0; c < C; c++) {
        if (img[r][c])
          underlying[r * C + c] = color;
        else
          underlying[r * C + c] = CRGB::Black;
      }
    }
  }
};

#endif // DARK_LIGHTS_IMAGE_TOOLS_H
