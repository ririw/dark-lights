/*
 Created by Richard Weiss on 2019-04-19.

 Algorithm / approach
 --------------------

 1. Read z-acceleration values into a ring buffer (RingBuffer class)
 2. Calculate a baseline acceleration in the z-direction, representing gravity (RingBuffer.ring_median)
 3. Calculate the acceleration, minus the baseline acceleration
 4. Take the dot-product of acceleration with an exponential kernel. This helps avoid "jumping into the floor"
    would occur with a standard rolling sum. With a rolling sum there is a jump up as the jump enters the
    ring buffer, and a jump into the floor as it exits the ring buffer. With the exp-kernel, the down-jump
    is much smaller.

 Further details
 ---------------
 - The ring-buffer class handles the indexing, so that selecting index -1 would
   return the most recent point.
 - The ring buffer median is done with the quickselect algorithm. However, it
   only does it on 10% of the ring buffer data, because otherwise it's too slow.
 - The exp stuff is done with dot and the eigen library.

 Tuning knobs
 ------------
 - RING_SIZE: set this to contain at least a second or two of data, to ensure there is a good estimate
   of the baseline acceleration.
 - Kernel shape: there's a lot of value in tweaking this, but I have no guidance on how.
 - The 10% proportion used in median filtering. For small ring-sizes, make this bigger,
   and for large ring sizes make it smaller :).
*/

#ifndef DARK_LIGHTS_ACCEL_TOOLS_H
#define DARK_LIGHTS_ACCEL_TOOLS_H

#include <Arduino.h>
#include <Eigen.h>
#include <math.h>

inline void swap(int *v, int a, int b) {
  int tmp = v[a];
  v[a] = v[b];
  v[b] = tmp;
}

int qselect(int *v, int len, int k) {
  int i, st;

  for (st = i = 0; i < len - 1; i++) {
    if (v[i] > v[len - 1])
      continue;
    swap(v, i, st);
    st++;
  }

  swap(v, len - 1, st);

  if (k == st) {
    return v[st];
  } else {
    if (st > k)
      return qselect(v, st, k);
    else
      return qselect(v + st, len - st, k - st);
  }
}

template <uint16_t RING_SIZE> class RingBuffer {
private:
  int ring[RING_SIZE] = {0};
  uint16_t ring_ptr = 0;

public:
  void push(int z) {
    ring[ring_ptr] = z;
    ring_ptr = (ring_ptr + 1) % RING_SIZE;
  }

  float ring_mean() {
    float total = 0;
    for (auto a : ring)
      total += a;
    return total / RING_SIZE;
  }

  int ring_median() {
    int qs_ring[RING_SIZE / 10];
    for (int i = 0; i < RING_SIZE / 10; i++) {
      qs_ring[i] = ring[i * 10];
    }
    return qselect(qs_ring, RING_SIZE / 10, RING_SIZE / 20);
  }

  int &operator[](const int index) {
    return ring[(index + ring_ptr) % RING_SIZE];
  }
};

template <uint16_t RING_SIZE> class JumpDetector {
public:
  RingBuffer<RING_SIZE> ring = RingBuffer<RING_SIZE>();
  Eigen::Matrix<float, RING_SIZE, 1> kernel;
  Eigen::Matrix<float, RING_SIZE, 1> current_ring;

  JumpDetector() {
    for (int i = 0; i < RING_SIZE; i += 1) {
      float i_f = i;
      kernel[i] = expf(i_f / RING_SIZE * 4);
    }
    kernel /= kernel.sum();
  }

  float calculate_velocity(int z) {
    ring.push(z);
    float ring_median = ring.ring_median();
    for (int i = 0; i < RING_SIZE; i++)
      current_ring[i] = ring[i] - ring_median;
    float res = current_ring.dot(kernel);
    return res;
  }


};

#endif // DARK_LIGHTS_ACCEL_TOOLS_H
